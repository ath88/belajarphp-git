<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pegawai extends CI_Controller {

	public function index()
	{
		$this->load->model('Models_pegawai','',TRUE);
		$datas = $this->Models_pegawai->get();
		//print_r($datas);
		$siapa['kamu'] = $datas;
		$this->load->view('pegawai',$siapa);
	}

	public function form()
	{
		$this->load->view('pegawai_form');
	}

	public function simpan()
	{
		$this->load->model('Models_pegawai','',TRUE);
		$nik = $this->input->post('nik');
		$nama = $this->input->post('nama');
		$jeniskelamin = $this->input->post('jenis_kelamin');

		$param = array(
			'nik'=>$nik,
			'nama'=>$nama,
			'jenis_kelamin'=>$jeniskelamin
		);

		$proses = $this->Models_pegawai->insert('pegawai',$param);
		if($proses)
			echo "Input Berhasil";
		else
			echo "Input Gagal";

		//print_r($param);
	}

	public function simpan_ubah()
	{
		$this->load->model('Models_pegawai','',TRUE);
		$nik_old = $this->input->post('nik_old');
		$nik = $this->input->post('nik');
		$nama = $this->input->post('nama');
		$jeniskelamin = $this->input->post('jenis_kelamin');

		$param = array(
			'nik'=>$nik,
			'nama'=>$nama,
			'jenis_kelamin'=>$jeniskelamin
		);
		$key = array('nik'=>$nik_old);

		$proses = $this->Models_pegawai->update('pegawai',$param,$key);
		if($proses)
			echo "Update Berhasil";
		else
			echo "Update Gagal";

		//print_r($param);
	}

	public function ubah()
	{
		$this->load->model('Models_pegawai','',TRUE);
		$id = $this->uri->segment(3);
		$key = array('nik'=>$id);

		$data['datas'] = $this->Models_pegawai->get_by_id('pegawai',$key);

		$this->load->view('pegawai_form_ubah',$data);
	}

	public function hapus()
	{
		$this->load->model('Models_pegawai','',TRUE);
		$id = $this->uri->segment(3);
		$key = array('nik'=>$id);

		$proses = $this->Models_pegawai->delete('history_jabatan',$key);
		$proses = $this->Models_pegawai->delete('pegawai',$key);
		if($proses)
			echo "Hapus Berhasil";
		else
			echo "Hapus Gagal";
	}
}
