/*
SQLyog Ultimate v12.4.3 (64 bit)
MySQL - 10.1.28-MariaDB : Database - pegawai
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`pegawai` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `pegawai`;

/*Table structure for table `history_jabatan` */

DROP TABLE IF EXISTS `history_jabatan`;

CREATE TABLE `history_jabatan` (
  `id_history` int(11) NOT NULL AUTO_INCREMENT,
  `nik` varchar(100) NOT NULL,
  `kode_jabatan` int(11) NOT NULL,
  `tmt_jabatan` date NOT NULL,
  PRIMARY KEY (`id_history`),
  KEY `kode_jabatan` (`kode_jabatan`),
  KEY `nik` (`nik`),
  CONSTRAINT `history_jabatan_ibfk_1` FOREIGN KEY (`kode_jabatan`) REFERENCES `jabatan` (`kodejabatan`) ON UPDATE CASCADE,
  CONSTRAINT `history_jabatan_ibfk_2` FOREIGN KEY (`nik`) REFERENCES `pegawai` (`nik`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `history_jabatan` */

/*Table structure for table `jabatan` */

DROP TABLE IF EXISTS `jabatan`;

CREATE TABLE `jabatan` (
  `kodejabatan` int(11) NOT NULL AUTO_INCREMENT,
  `namajabatan` varchar(100) NOT NULL,
  PRIMARY KEY (`kodejabatan`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `jabatan` */

insert  into `jabatan`(`kodejabatan`,`namajabatan`) values 
(1,'Kepala Dinas'),
(2,'Kepala Bagian');

/*Table structure for table `keys` */

DROP TABLE IF EXISTS `keys`;

CREATE TABLE `keys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `key` varchar(40) NOT NULL,
  `level` int(2) NOT NULL,
  `ignore_limits` tinyint(1) NOT NULL DEFAULT '0',
  `is_private_key` tinyint(1) NOT NULL DEFAULT '0',
  `ip_addresses` text,
  `date_created` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `keys` */

insert  into `keys`(`id`,`user_id`,`key`,`level`,`ignore_limits`,`is_private_key`,`ip_addresses`,`date_created`) values 
(1,1,'GIZBerau@2019',0,0,0,NULL,2019);

/*Table structure for table `logs` */

DROP TABLE IF EXISTS `logs`;

CREATE TABLE `logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uri` varchar(255) NOT NULL,
  `method` varchar(6) NOT NULL,
  `params` text,
  `api_key` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `time` int(11) NOT NULL,
  `rtime` float DEFAULT NULL,
  `authorized` varchar(1) NOT NULL,
  `response_code` smallint(3) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8;

/*Data for the table `logs` */

insert  into `logs`(`id`,`uri`,`method`,`params`,`api_key`,`ip_address`,`time`,`rtime`,`authorized`,`response_code`) values 
(1,'pegawai/index/id/64710524109900','get','a:9:{s:2:\"id\";s:14:\"64710524109900\";s:13:\"cache-control\";s:8:\"no-cache\";s:13:\"Postman-Token\";s:36:\"c0c680ce-2485-4883-a219-fb1a393a42b9\";s:13:\"Authorization\";s:26:\"Basic YWRtaW46MTEyMjMzNDU=\";s:10:\"User-Agent\";s:20:\"PostmanRuntime/7.6.1\";s:6:\"Accept\";s:3:\"*/*\";s:4:\"Host\";s:9:\"localhost\";s:15:\"accept-encoding\";s:13:\"gzip, deflate\";s:10:\"Connection\";s:10:\"keep-alive\";}','','::1',1558622429,NULL,'0',0),
(2,'pegawai/index','get','a:8:{s:13:\"cache-control\";s:8:\"no-cache\";s:13:\"Postman-Token\";s:36:\"83c08e77-d46f-4bbb-b0bf-f9e23df2a233\";s:13:\"Authorization\";s:26:\"Basic YWRtaW46MTEyMjMzNDU=\";s:10:\"User-Agent\";s:20:\"PostmanRuntime/7.6.1\";s:6:\"Accept\";s:3:\"*/*\";s:4:\"Host\";s:9:\"localhost\";s:15:\"accept-encoding\";s:13:\"gzip, deflate\";s:10:\"Connection\";s:10:\"keep-alive\";}','','::1',1558622472,NULL,'0',0),
(3,'pegawai/index','get','a:8:{s:13:\"cache-control\";s:8:\"no-cache\";s:13:\"Postman-Token\";s:36:\"de63297e-0202-46d5-a978-1c5383fdb0c7\";s:13:\"Authorization\";s:26:\"Basic YWRtaW46MTEyMjMzNDU=\";s:10:\"User-Agent\";s:20:\"PostmanRuntime/7.6.1\";s:6:\"Accept\";s:3:\"*/*\";s:4:\"Host\";s:9:\"localhost\";s:15:\"accept-encoding\";s:13:\"gzip, deflate\";s:10:\"Connection\";s:10:\"keep-alive\";}','','::1',1558622623,NULL,'1',0),
(4,'pegawai/index','get','a:9:{s:9:\"x-api-key\";s:13:\"GIZBerau@2019\";s:13:\"cache-control\";s:8:\"no-cache\";s:13:\"Postman-Token\";s:36:\"dfcd7521-fa35-4efd-a183-976d1ad61377\";s:13:\"Authorization\";s:26:\"Basic YWRtaW46MTEyMjMzNDU=\";s:10:\"User-Agent\";s:20:\"PostmanRuntime/7.6.1\";s:6:\"Accept\";s:3:\"*/*\";s:4:\"Host\";s:9:\"localhost\";s:15:\"accept-encoding\";s:13:\"gzip, deflate\";s:10:\"Connection\";s:10:\"keep-alive\";}','GIZBerau@2019','::1',1558622731,NULL,'1',0),
(5,'pegawai/index','get','a:9:{s:9:\"x-api-key\";s:13:\"GIZBerau@2019\";s:13:\"cache-control\";s:8:\"no-cache\";s:13:\"Postman-Token\";s:36:\"baaee260-fde6-4451-a2b6-20ae792774ab\";s:13:\"Authorization\";s:26:\"Basic YWRtaW46MTEyMjMzNDU=\";s:10:\"User-Agent\";s:20:\"PostmanRuntime/7.6.1\";s:6:\"Accept\";s:3:\"*/*\";s:4:\"Host\";s:9:\"localhost\";s:15:\"accept-encoding\";s:13:\"gzip, deflate\";s:10:\"Connection\";s:10:\"keep-alive\";}','GIZBerau@2019','::1',1558622741,NULL,'1',0),
(6,'pegawai/index','get','a:8:{s:13:\"cache-control\";s:8:\"no-cache\";s:13:\"Postman-Token\";s:36:\"4f6cd18f-6ea1-4634-a370-e42dfb9e892a\";s:13:\"Authorization\";s:26:\"Basic YWRtaW46MTEyMjMzNDU=\";s:10:\"User-Agent\";s:20:\"PostmanRuntime/7.6.1\";s:6:\"Accept\";s:3:\"*/*\";s:4:\"Host\";s:9:\"localhost\";s:15:\"accept-encoding\";s:13:\"gzip, deflate\";s:10:\"Connection\";s:10:\"keep-alive\";}','','::1',1558622796,NULL,'0',0),
(7,'pegawai/index','get','a:9:{s:9:\"x-api-key\";s:13:\"GIZBerau@2019\";s:13:\"cache-control\";s:8:\"no-cache\";s:13:\"Postman-Token\";s:36:\"5d8171f2-b975-4baf-a45b-1b8a30d303df\";s:13:\"Authorization\";s:26:\"Basic YWRtaW46MTEyMjMzNDU=\";s:10:\"User-Agent\";s:20:\"PostmanRuntime/7.6.1\";s:6:\"Accept\";s:3:\"*/*\";s:4:\"Host\";s:9:\"localhost\";s:15:\"accept-encoding\";s:13:\"gzip, deflate\";s:10:\"Connection\";s:10:\"keep-alive\";}','GIZBerau@2019','::1',1558622801,NULL,'1',0),
(8,'pegawai/index','get','a:9:{s:9:\"x-api-key\";s:13:\"GIZBerau@2019\";s:13:\"cache-control\";s:8:\"no-cache\";s:13:\"Postman-Token\";s:36:\"0e8ef99b-2c6a-4284-b85e-57853037ec5d\";s:13:\"Authorization\";s:26:\"Basic YWRtaW46MTEyMjMzNDU=\";s:10:\"User-Agent\";s:20:\"PostmanRuntime/7.6.1\";s:6:\"Accept\";s:3:\"*/*\";s:4:\"Host\";s:9:\"localhost\";s:15:\"accept-encoding\";s:13:\"gzip, deflate\";s:10:\"Connection\";s:10:\"keep-alive\";}','GIZBerau@2019','::1',1558622837,NULL,'1',0),
(9,'pegawai/index','get','a:9:{s:9:\"x-api-key\";s:13:\"GIZBerau@2019\";s:13:\"cache-control\";s:8:\"no-cache\";s:13:\"Postman-Token\";s:36:\"83589c6c-3789-4633-add7-85dcc2295df1\";s:13:\"Authorization\";s:26:\"Basic YWRtaW46MTEyMjMzNDU=\";s:10:\"User-Agent\";s:20:\"PostmanRuntime/7.6.1\";s:6:\"Accept\";s:3:\"*/*\";s:4:\"Host\";s:9:\"localhost\";s:15:\"accept-encoding\";s:13:\"gzip, deflate\";s:10:\"Connection\";s:10:\"keep-alive\";}','GIZBerau@2019','::1',1558622877,NULL,'1',0),
(10,'pegawai/index','get','a:8:{s:13:\"cache-control\";s:8:\"no-cache\";s:13:\"Postman-Token\";s:36:\"005beb73-fa2c-4333-a300-984d84a7eb1e\";s:13:\"Authorization\";s:26:\"Basic YWRtaW46MTEyMjMzNDU=\";s:10:\"User-Agent\";s:20:\"PostmanRuntime/7.6.1\";s:6:\"Accept\";s:3:\"*/*\";s:4:\"Host\";s:9:\"localhost\";s:15:\"accept-encoding\";s:13:\"gzip, deflate\";s:10:\"Connection\";s:10:\"keep-alive\";}','','::1',1558622895,NULL,'1',0),
(11,'pegawai/index','get','a:9:{s:9:\"x-api-key\";s:13:\"GIZBerau@2019\";s:13:\"cache-control\";s:8:\"no-cache\";s:13:\"Postman-Token\";s:36:\"3ca772f0-52d9-40d6-89fa-ae60c9c42a93\";s:13:\"Authorization\";s:26:\"Basic YWRtaW46MTEyMjMzNDU=\";s:10:\"User-Agent\";s:20:\"PostmanRuntime/7.6.1\";s:6:\"Accept\";s:3:\"*/*\";s:4:\"Host\";s:9:\"localhost\";s:15:\"accept-encoding\";s:13:\"gzip, deflate\";s:10:\"Connection\";s:10:\"keep-alive\";}','','::1',1558622921,NULL,'1',0),
(12,'pegawai/index/id/64710524109900','get','a:10:{s:2:\"id\";s:14:\"64710524109900\";s:9:\"x-api-key\";s:13:\"GIZBerau@2019\";s:13:\"cache-control\";s:8:\"no-cache\";s:13:\"Postman-Token\";s:36:\"4c1fea99-1909-4e56-853a-429432813a42\";s:13:\"Authorization\";s:26:\"Basic YWRtaW46MTEyMjMzNDU=\";s:10:\"User-Agent\";s:20:\"PostmanRuntime/7.6.1\";s:6:\"Accept\";s:3:\"*/*\";s:4:\"Host\";s:9:\"localhost\";s:15:\"accept-encoding\";s:13:\"gzip, deflate\";s:10:\"Connection\";s:10:\"keep-alive\";}','','::1',1558622939,NULL,'1',0),
(13,'pegawai','post','a:14:{s:12:\"Content-Type\";s:33:\"application/x-www-form-urlencoded\";s:9:\"x-api-key\";s:13:\"GIZBerau@2019\";s:13:\"cache-control\";s:8:\"no-cache\";s:13:\"Postman-Token\";s:36:\"0bcf9e67-456f-4fff-8ba2-adc9a6ca7886\";s:13:\"Authorization\";s:26:\"Basic YWRtaW46MTEyMjMzNDU=\";s:10:\"User-Agent\";s:20:\"PostmanRuntime/7.6.1\";s:6:\"Accept\";s:3:\"*/*\";s:4:\"Host\";s:9:\"localhost\";s:15:\"accept-encoding\";s:13:\"gzip, deflate\";s:14:\"content-length\";s:2:\"43\";s:10:\"Connection\";s:10:\"keep-alive\";s:3:\"nik\";s:6:\"112233\";s:4:\"nama\";s:9:\"GIZ Berau\";s:13:\"jenis_kelamin\";s:1:\"L\";}','','::1',1558622962,NULL,'1',0),
(14,'pegawai','put','a:15:{s:12:\"Content-Type\";s:33:\"application/x-www-form-urlencoded\";s:9:\"x-api-key\";s:13:\"GIZBerau@2019\";s:13:\"cache-control\";s:8:\"no-cache\";s:13:\"Postman-Token\";s:36:\"9529f5e1-c5c5-46b7-86c6-6a60493ac236\";s:13:\"Authorization\";s:26:\"Basic YWRtaW46MTEyMjMzNDU=\";s:10:\"User-Agent\";s:20:\"PostmanRuntime/7.6.1\";s:6:\"Accept\";s:3:\"*/*\";s:4:\"Host\";s:9:\"localhost\";s:15:\"accept-encoding\";s:13:\"gzip, deflate\";s:14:\"content-length\";s:2:\"57\";s:10:\"Connection\";s:10:\"keep-alive\";s:3:\"nik\";s:7:\"1122331\";s:4:\"nama\";s:12:\"GIZ Berauuuu\";s:13:\"jenis_kelamin\";s:1:\"P\";s:2:\"id\";s:6:\"112233\";}','','::1',1558622976,NULL,'1',0),
(15,'pegawai','delete','a:12:{s:12:\"Content-Type\";s:33:\"application/x-www-form-urlencoded\";s:9:\"x-api-key\";s:0:\"\";s:13:\"cache-control\";s:8:\"no-cache\";s:13:\"Postman-Token\";s:36:\"1240c2fb-010c-4cc2-8a44-e7f91383b662\";s:13:\"Authorization\";s:26:\"Basic YWRtaW46MTEyMjMzNDU=\";s:10:\"User-Agent\";s:20:\"PostmanRuntime/7.6.1\";s:6:\"Accept\";s:3:\"*/*\";s:4:\"Host\";s:9:\"localhost\";s:15:\"accept-encoding\";s:13:\"gzip, deflate\";s:14:\"content-length\";s:2:\"10\";s:10:\"Connection\";s:10:\"keep-alive\";s:2:\"id\";s:7:\"1122331\";}','','::1',1558622989,NULL,'1',0),
(16,'pegawai','put','a:14:{s:12:\"Content-Type\";s:33:\"application/x-www-form-urlencoded\";s:13:\"cache-control\";s:8:\"no-cache\";s:13:\"Postman-Token\";s:36:\"22c3e8e2-3e19-4ab5-868d-cf88c938e45c\";s:13:\"Authorization\";s:26:\"Basic YWRtaW46MTEyMjMzNDU=\";s:10:\"User-Agent\";s:20:\"PostmanRuntime/7.6.1\";s:6:\"Accept\";s:3:\"*/*\";s:4:\"Host\";s:9:\"localhost\";s:15:\"accept-encoding\";s:13:\"gzip, deflate\";s:14:\"content-length\";s:2:\"57\";s:10:\"Connection\";s:10:\"keep-alive\";s:3:\"nik\";s:7:\"1122331\";s:4:\"nama\";s:12:\"GIZ Berauuuu\";s:13:\"jenis_kelamin\";s:1:\"P\";s:2:\"id\";s:6:\"112233\";}','','::1',1558623073,NULL,'1',0),
(17,'pegawai','put','a:14:{s:12:\"Content-Type\";s:33:\"application/x-www-form-urlencoded\";s:13:\"cache-control\";s:8:\"no-cache\";s:13:\"Postman-Token\";s:36:\"3a77e78d-9acc-4b44-bad3-35605469bd99\";s:13:\"Authorization\";s:26:\"Basic YWRtaW46MTEyMjMzNDU=\";s:10:\"User-Agent\";s:20:\"PostmanRuntime/7.6.1\";s:6:\"Accept\";s:3:\"*/*\";s:4:\"Host\";s:9:\"localhost\";s:15:\"accept-encoding\";s:13:\"gzip, deflate\";s:14:\"content-length\";s:2:\"57\";s:10:\"Connection\";s:10:\"keep-alive\";s:3:\"nik\";s:7:\"1122331\";s:4:\"nama\";s:12:\"GIZ Berauuuu\";s:13:\"jenis_kelamin\";s:1:\"P\";s:2:\"id\";s:6:\"112233\";}','','::1',1558623095,NULL,'0',0),
(18,'pegawai','put','a:15:{s:12:\"Content-Type\";s:33:\"application/x-www-form-urlencoded\";s:9:\"x-api-key\";s:13:\"GIZBerau@2019\";s:13:\"cache-control\";s:8:\"no-cache\";s:13:\"Postman-Token\";s:36:\"c172ed87-2f52-42b3-a49b-6d2f2772df0b\";s:13:\"Authorization\";s:26:\"Basic YWRtaW46MTEyMjMzNDU=\";s:10:\"User-Agent\";s:20:\"PostmanRuntime/7.6.1\";s:6:\"Accept\";s:3:\"*/*\";s:4:\"Host\";s:9:\"localhost\";s:15:\"accept-encoding\";s:13:\"gzip, deflate\";s:14:\"content-length\";s:2:\"57\";s:10:\"Connection\";s:10:\"keep-alive\";s:3:\"nik\";s:7:\"1122331\";s:4:\"nama\";s:12:\"GIZ Berauuuu\";s:13:\"jenis_kelamin\";s:1:\"P\";s:2:\"id\";s:6:\"112233\";}','GIZBerau@2019','::1',1558623098,NULL,'1',0),
(19,'pegawai/index/id/64710524109900','get','a:10:{s:2:\"id\";s:14:\"64710524109900\";s:9:\"x-api-key\";s:13:\"GIZBerau@2019\";s:13:\"cache-control\";s:8:\"no-cache\";s:13:\"Postman-Token\";s:36:\"d8a2b740-ddf5-417b-9b2d-a89783c85246\";s:13:\"Authorization\";s:26:\"Basic YWRtaW46MTEyMjMzNDU=\";s:10:\"User-Agent\";s:20:\"PostmanRuntime/7.6.1\";s:6:\"Accept\";s:3:\"*/*\";s:4:\"Host\";s:9:\"localhost\";s:15:\"accept-encoding\";s:13:\"gzip, deflate\";s:10:\"Connection\";s:10:\"keep-alive\";}','GIZBerau@2019','::1',1558623104,NULL,'1',0),
(20,'pegawai','post','a:14:{s:12:\"Content-Type\";s:33:\"application/x-www-form-urlencoded\";s:9:\"x-api-key\";s:13:\"GIZBerau@2019\";s:13:\"cache-control\";s:8:\"no-cache\";s:13:\"Postman-Token\";s:36:\"2f8ce3ac-69a7-49f7-83ee-ec1e29152b4a\";s:13:\"Authorization\";s:26:\"Basic YWRtaW46MTEyMjMzNDU=\";s:10:\"User-Agent\";s:20:\"PostmanRuntime/7.6.1\";s:6:\"Accept\";s:3:\"*/*\";s:4:\"Host\";s:9:\"localhost\";s:15:\"accept-encoding\";s:13:\"gzip, deflate\";s:14:\"content-length\";s:2:\"43\";s:10:\"Connection\";s:10:\"keep-alive\";s:3:\"nik\";s:6:\"112233\";s:4:\"nama\";s:9:\"GIZ Berau\";s:13:\"jenis_kelamin\";s:1:\"L\";}','GIZBerau@2019','::1',1558623107,NULL,'1',0),
(21,'pegawai/index/id/64710524109900','get','a:10:{s:2:\"id\";s:14:\"64710524109900\";s:9:\"x-api-key\";s:13:\"GIZBerau@2019\";s:13:\"cache-control\";s:8:\"no-cache\";s:13:\"Postman-Token\";s:36:\"5f7798a4-66ec-4847-ac49-ba84595feaec\";s:13:\"Authorization\";s:26:\"Basic YWRtaW46MTEyMjMzNDU=\";s:10:\"User-Agent\";s:20:\"PostmanRuntime/7.6.1\";s:6:\"Accept\";s:3:\"*/*\";s:4:\"Host\";s:9:\"localhost\";s:15:\"accept-encoding\";s:13:\"gzip, deflate\";s:10:\"Connection\";s:10:\"keep-alive\";}','GIZBerau@2019','::1',1558623109,NULL,'1',0),
(22,'pegawai/index','get','a:9:{s:9:\"x-api-key\";s:13:\"GIZBerau@2019\";s:13:\"cache-control\";s:8:\"no-cache\";s:13:\"Postman-Token\";s:36:\"e77e7435-783a-4769-abda-1970b7351a0a\";s:13:\"Authorization\";s:26:\"Basic YWRtaW46MTEyMjMzNDU=\";s:10:\"User-Agent\";s:20:\"PostmanRuntime/7.6.1\";s:6:\"Accept\";s:3:\"*/*\";s:4:\"Host\";s:9:\"localhost\";s:15:\"accept-encoding\";s:13:\"gzip, deflate\";s:10:\"Connection\";s:10:\"keep-alive\";}','GIZBerau@2019','::1',1558623112,NULL,'1',0),
(23,'pegawai/index','get','a:8:{s:13:\"cache-control\";s:8:\"no-cache\";s:13:\"Postman-Token\";s:36:\"251d645d-9321-4e9f-8355-55d91da67f12\";s:13:\"Authorization\";s:26:\"Basic YWRtaW46MTEyMjMzNDU=\";s:10:\"User-Agent\";s:20:\"PostmanRuntime/7.6.1\";s:6:\"Accept\";s:3:\"*/*\";s:4:\"Host\";s:9:\"localhost\";s:15:\"accept-encoding\";s:13:\"gzip, deflate\";s:10:\"Connection\";s:10:\"keep-alive\";}','','::1',1558623115,NULL,'0',0),
(24,'pegawai/index','get','a:9:{s:9:\"x-api-key\";s:13:\"GIZBerau@2019\";s:13:\"cache-control\";s:8:\"no-cache\";s:13:\"Postman-Token\";s:36:\"bf854cb6-ee02-42e4-8f9d-a6114bf71573\";s:13:\"Authorization\";s:26:\"Basic YWRtaW46MTEyMjMzNDU=\";s:10:\"User-Agent\";s:20:\"PostmanRuntime/7.6.1\";s:6:\"Accept\";s:3:\"*/*\";s:4:\"Host\";s:9:\"localhost\";s:15:\"accept-encoding\";s:13:\"gzip, deflate\";s:10:\"Connection\";s:10:\"keep-alive\";}','GIZBerau@2019','::1',1558623118,NULL,'1',0),
(25,'pegawai/index','get','a:4:{s:4:\"Host\";s:9:\"localhost\";s:13:\"Authorization\";s:26:\"Basic YWRtaW46MTEyMjMzNDU=\";s:6:\"Accept\";s:3:\"*/*\";s:9:\"X-API-KEY\";s:13:\"GIZBerau@2019\";}','GIZBerau@2019','::1',1558624305,NULL,'1',0),
(26,'pegawai/index','get','a:4:{s:4:\"Host\";s:9:\"localhost\";s:13:\"Authorization\";s:26:\"Basic YWRtaW46MTEyMjMzNDU=\";s:6:\"Accept\";s:3:\"*/*\";s:9:\"X-API-KEY\";s:13:\"GIZBerau@2019\";}','GIZBerau@2019','::1',1558624323,NULL,'1',0),
(27,'pegawai/index','get','a:4:{s:4:\"Host\";s:9:\"localhost\";s:13:\"Authorization\";s:26:\"Basic YWRtaW46MTEyMjMzNDU=\";s:6:\"Accept\";s:3:\"*/*\";s:9:\"X-API-KEY\";s:13:\"GIZBerau@2019\";}','GIZBerau@2019','::1',1558624390,NULL,'1',0),
(28,'pegawai/index/id/112233','get','a:5:{s:2:\"id\";s:6:\"112233\";s:4:\"Host\";s:9:\"localhost\";s:13:\"Authorization\";s:26:\"Basic YWRtaW46MTEyMjMzNDU=\";s:6:\"Accept\";s:3:\"*/*\";s:9:\"X-API-KEY\";s:13:\"GIZBerau@2019\";}','GIZBerau@2019','::1',1558624390,NULL,'1',0),
(29,'pegawai/index','get','a:4:{s:4:\"Host\";s:9:\"localhost\";s:13:\"Authorization\";s:26:\"Basic YWRtaW46MTEyMjMzNDU=\";s:6:\"Accept\";s:3:\"*/*\";s:9:\"X-API-KEY\";s:13:\"GIZBerau@2019\";}','GIZBerau@2019','::1',1558624398,NULL,'1',0),
(30,'pegawai/index/id/112233','get','a:5:{s:2:\"id\";s:6:\"112233\";s:4:\"Host\";s:9:\"localhost\";s:13:\"Authorization\";s:26:\"Basic YWRtaW46MTEyMjMzNDU=\";s:6:\"Accept\";s:3:\"*/*\";s:9:\"X-API-KEY\";s:13:\"GIZBerau@2019\";}','GIZBerau@2019','::1',1558624398,NULL,'1',0),
(31,'pegawai/index','get','a:4:{s:4:\"Host\";s:9:\"localhost\";s:13:\"Authorization\";s:26:\"Basic YWRtaW46MTEyMjMzNDU=\";s:6:\"Accept\";s:3:\"*/*\";s:9:\"X-API-KEY\";s:13:\"GIZBerau@2019\";}','GIZBerau@2019','::1',1558624701,NULL,'1',0),
(32,'pegawai/index','get','a:4:{s:4:\"Host\";s:9:\"localhost\";s:13:\"Authorization\";s:26:\"Basic YWRtaW46MTEyMjMzNDU=\";s:6:\"Accept\";s:3:\"*/*\";s:9:\"X-API-KEY\";s:13:\"GIZBerau@2019\";}','GIZBerau@2019','::1',1558624701,NULL,'1',0),
(33,'pegawai/index','put','a:10:{s:4:\"Host\";s:9:\"localhost\";s:13:\"Authorization\";s:26:\"Basic YWRtaW46MTEyMjMzNDU=\";s:6:\"Accept\";s:3:\"*/*\";s:9:\"X-API-KEY\";s:13:\"GIZBerau@2019\";s:12:\"Content-Type\";s:33:\"application/x-www-form-urlencoded\";s:14:\"Content-Length\";s:2:\"58\";s:2:\"id\";s:5:\"54321\";s:3:\"nik\";s:6:\"543211\";s:4:\"nama\";s:17:\"TEST POST API PUT\";s:13:\"jenis_kelamin\";s:1:\"L\";}','GIZBerau@2019','::1',1558624701,NULL,'1',0),
(34,'pegawai/index/id/543211','get','a:5:{s:2:\"id\";s:6:\"543211\";s:4:\"Host\";s:9:\"localhost\";s:13:\"Authorization\";s:26:\"Basic YWRtaW46MTEyMjMzNDU=\";s:6:\"Accept\";s:3:\"*/*\";s:9:\"X-API-KEY\";s:13:\"GIZBerau@2019\";}','GIZBerau@2019','::1',1558624701,NULL,'1',0),
(35,'pegawai/index','delete','a:7:{s:4:\"Host\";s:9:\"localhost\";s:13:\"Authorization\";s:26:\"Basic YWRtaW46MTEyMjMzNDU=\";s:6:\"Accept\";s:3:\"*/*\";s:9:\"X-API-KEY\";s:13:\"GIZBerau@2019\";s:12:\"Content-Type\";s:33:\"application/x-www-form-urlencoded\";s:14:\"Content-Length\";s:1:\"9\";s:2:\"id\";s:6:\"543211\";}','GIZBerau@2019','::1',1558624701,NULL,'1',0),
(36,'pegawai/index','get','a:4:{s:4:\"Host\";s:9:\"localhost\";s:13:\"Authorization\";s:26:\"Basic YWRtaW46MTEyMjMzNDU=\";s:6:\"Accept\";s:3:\"*/*\";s:9:\"X-API-KEY\";s:13:\"GIZBerau@2019\";}','GIZBerau@2019','::1',1558624822,NULL,'1',0),
(37,'pegawai/index','get','a:4:{s:4:\"Host\";s:9:\"localhost\";s:13:\"Authorization\";s:26:\"Basic YWRtaW46MTEyMjMzNDU=\";s:6:\"Accept\";s:3:\"*/*\";s:9:\"X-API-KEY\";s:13:\"GIZBerau@2019\";}','GIZBerau@2019','::1',1558624822,NULL,'1',0),
(38,'pegawai/index','put','a:10:{s:4:\"Host\";s:9:\"localhost\";s:13:\"Authorization\";s:26:\"Basic YWRtaW46MTEyMjMzNDU=\";s:6:\"Accept\";s:3:\"*/*\";s:9:\"X-API-KEY\";s:13:\"GIZBerau@2019\";s:12:\"Content-Type\";s:33:\"application/x-www-form-urlencoded\";s:14:\"Content-Length\";s:2:\"58\";s:2:\"id\";s:5:\"54321\";s:3:\"nik\";s:6:\"543211\";s:4:\"nama\";s:17:\"TEST POST API PUT\";s:13:\"jenis_kelamin\";s:1:\"L\";}','GIZBerau@2019','::1',1558624822,NULL,'1',0),
(39,'pegawai/index/id/543211','get','a:5:{s:2:\"id\";s:6:\"543211\";s:4:\"Host\";s:9:\"localhost\";s:13:\"Authorization\";s:26:\"Basic YWRtaW46MTEyMjMzNDU=\";s:6:\"Accept\";s:3:\"*/*\";s:9:\"X-API-KEY\";s:13:\"GIZBerau@2019\";}','GIZBerau@2019','::1',1558624822,NULL,'1',0),
(40,'pegawai/index','delete','a:7:{s:4:\"Host\";s:9:\"localhost\";s:13:\"Authorization\";s:26:\"Basic YWRtaW46MTEyMjMzNDU=\";s:6:\"Accept\";s:3:\"*/*\";s:9:\"X-API-KEY\";s:13:\"GIZBerau@2019\";s:12:\"Content-Type\";s:33:\"application/x-www-form-urlencoded\";s:14:\"Content-Length\";s:1:\"9\";s:2:\"id\";s:6:\"543211\";}','GIZBerau@2019','::1',1558624822,NULL,'1',0),
(41,'pegawai/index','get','a:4:{s:4:\"Host\";s:9:\"localhost\";s:13:\"Authorization\";s:26:\"Basic YWRtaW46MTEyMjMzNDU=\";s:6:\"Accept\";s:3:\"*/*\";s:9:\"X-API-KEY\";s:13:\"GIZBerau@2019\";}','GIZBerau@2019','::1',1558624881,NULL,'1',0),
(42,'pegawai','get','a:4:{s:4:\"Host\";s:9:\"localhost\";s:13:\"Authorization\";s:26:\"Basic YWRtaW46MTEyMjMzNDU=\";s:6:\"Accept\";s:3:\"*/*\";s:9:\"X-API-KEY\";s:13:\"GIZBerau@2019\";}','GIZBerau@2019','::1',1558624881,NULL,'1',0),
(43,'pegawai/index','post','a:10:{s:4:\"Host\";s:9:\"localhost\";s:13:\"Authorization\";s:26:\"Basic YWRtaW46MTEyMjMzNDU=\";s:6:\"Accept\";s:3:\"*/*\";s:9:\"X-API-KEY\";s:13:\"GIZBerau@2019\";s:14:\"Content-Length\";s:3:\"352\";s:6:\"Expect\";s:12:\"100-continue\";s:12:\"Content-Type\";s:84:\"application/x-www-form-urlencoded; boundary=------------------------2929d68e26d14fd6\";s:80:\"--------------------------2929d68e26d14fd6\r\nContent-Disposition:_form-data;_name\";s:271:\"\"nik\"\r\n\r\n54321\r\n--------------------------2929d68e26d14fd6\r\nContent-Disposition: form-data; name=\"nama\"\r\n\r\nTEST POST API\r\n--------------------------2929d68e26d14fd6\r\nContent-Disposition: form-data; name=\"jenis_kelamin\"\r\n\r\nL\r\n--------------------------2929d68e26d14fd6--\r\n\";i:0;s:271:\"\"nik\"\r\n\r\n54321\r\n--------------------------2929d68e26d14fd6\r\nContent-Disposition: form-data; name=\"nama\"\r\n\r\nTEST POST API\r\n--------------------------2929d68e26d14fd6\r\nContent-Disposition: form-data; name=\"jenis_kelamin\"\r\n\r\nL\r\n--------------------------2929d68e26d14fd6--\r\n\";i:1;s:271:\"\"nik\"\r\n\r\n54321\r\n--------------------------2929d68e26d14fd6\r\nContent-Disposition: form-data; name=\"nama\"\r\n\r\nTEST POST API\r\n--------------------------2929d68e26d14fd6\r\nContent-Disposition: form-data; name=\"jenis_kelamin\"\r\n\r\nL\r\n--------------------------2929d68e26d14fd6--\r\n\";}','GIZBerau@2019','::1',1558624942,NULL,'1',0),
(44,'pegawai','get','a:4:{s:4:\"Host\";s:9:\"localhost\";s:13:\"Authorization\";s:26:\"Basic YWRtaW46MTEyMjMzNDU=\";s:6:\"Accept\";s:3:\"*/*\";s:9:\"X-API-KEY\";s:13:\"GIZBerau@2019\";}','GIZBerau@2019','::1',1558624942,NULL,'1',0),
(45,'pegawai/index','put','a:10:{s:4:\"Host\";s:9:\"localhost\";s:13:\"Authorization\";s:26:\"Basic YWRtaW46MTEyMjMzNDU=\";s:6:\"Accept\";s:3:\"*/*\";s:9:\"X-API-KEY\";s:13:\"GIZBerau@2019\";s:12:\"Content-Type\";s:33:\"application/x-www-form-urlencoded\";s:14:\"Content-Length\";s:2:\"58\";s:2:\"id\";s:5:\"54321\";s:3:\"nik\";s:6:\"543211\";s:4:\"nama\";s:17:\"TEST POST API PUT\";s:13:\"jenis_kelamin\";s:1:\"L\";}','GIZBerau@2019','::1',1558624942,NULL,'1',0),
(46,'pegawai/index/id/543211','get','a:5:{s:2:\"id\";s:6:\"543211\";s:4:\"Host\";s:9:\"localhost\";s:13:\"Authorization\";s:26:\"Basic YWRtaW46MTEyMjMzNDU=\";s:6:\"Accept\";s:3:\"*/*\";s:9:\"X-API-KEY\";s:13:\"GIZBerau@2019\";}','GIZBerau@2019','::1',1558624942,NULL,'1',0),
(47,'pegawai/index','delete','a:7:{s:4:\"Host\";s:9:\"localhost\";s:13:\"Authorization\";s:26:\"Basic YWRtaW46MTEyMjMzNDU=\";s:6:\"Accept\";s:3:\"*/*\";s:9:\"X-API-KEY\";s:13:\"GIZBerau@2019\";s:12:\"Content-Type\";s:33:\"application/x-www-form-urlencoded\";s:14:\"Content-Length\";s:1:\"9\";s:2:\"id\";s:6:\"543211\";}','GIZBerau@2019','::1',1558624943,NULL,'1',0),
(48,'pegawai/index','post','a:10:{s:4:\"Host\";s:9:\"localhost\";s:13:\"Authorization\";s:26:\"Basic YWRtaW46MTEyMjMzNDU=\";s:6:\"Accept\";s:3:\"*/*\";s:9:\"X-API-KEY\";s:13:\"GIZBerau@2019\";s:14:\"Content-Length\";s:3:\"352\";s:6:\"Expect\";s:12:\"100-continue\";s:12:\"Content-Type\";s:70:\"multipart/form-data; boundary=------------------------925ad33cc99f8e5f\";s:3:\"nik\";s:5:\"54321\";s:4:\"nama\";s:13:\"TEST POST API\";s:13:\"jenis_kelamin\";s:1:\"L\";}','GIZBerau@2019','::1',1558624959,NULL,'1',0),
(49,'pegawai','get','a:4:{s:4:\"Host\";s:9:\"localhost\";s:13:\"Authorization\";s:26:\"Basic YWRtaW46MTEyMjMzNDU=\";s:6:\"Accept\";s:3:\"*/*\";s:9:\"X-API-KEY\";s:13:\"GIZBerau@2019\";}','GIZBerau@2019','::1',1558624959,NULL,'1',0),
(50,'pegawai/index','put','a:10:{s:4:\"Host\";s:9:\"localhost\";s:13:\"Authorization\";s:26:\"Basic YWRtaW46MTEyMjMzNDU=\";s:6:\"Accept\";s:3:\"*/*\";s:9:\"X-API-KEY\";s:13:\"GIZBerau@2019\";s:12:\"Content-Type\";s:33:\"application/x-www-form-urlencoded\";s:14:\"Content-Length\";s:2:\"58\";s:2:\"id\";s:5:\"54321\";s:3:\"nik\";s:6:\"543211\";s:4:\"nama\";s:17:\"TEST POST API PUT\";s:13:\"jenis_kelamin\";s:1:\"L\";}','GIZBerau@2019','::1',1558624959,NULL,'1',0),
(51,'pegawai/index/id/543211','get','a:5:{s:2:\"id\";s:6:\"543211\";s:4:\"Host\";s:9:\"localhost\";s:13:\"Authorization\";s:26:\"Basic YWRtaW46MTEyMjMzNDU=\";s:6:\"Accept\";s:3:\"*/*\";s:9:\"X-API-KEY\";s:13:\"GIZBerau@2019\";}','GIZBerau@2019','::1',1558624960,NULL,'1',0),
(52,'pegawai/index','delete','a:7:{s:4:\"Host\";s:9:\"localhost\";s:13:\"Authorization\";s:26:\"Basic YWRtaW46MTEyMjMzNDU=\";s:6:\"Accept\";s:3:\"*/*\";s:9:\"X-API-KEY\";s:13:\"GIZBerau@2019\";s:12:\"Content-Type\";s:33:\"application/x-www-form-urlencoded\";s:14:\"Content-Length\";s:1:\"9\";s:2:\"id\";s:6:\"543211\";}','GIZBerau@2019','::1',1558624960,NULL,'1',0),
(53,'pegawai','get','a:9:{s:4:\"Host\";s:9:\"localhost\";s:10:\"Connection\";s:10:\"keep-alive\";s:13:\"Authorization\";s:26:\"Basic YWRtaW46MTEyMjMzNDU=\";s:25:\"Upgrade-Insecure-Requests\";s:1:\"1\";s:10:\"User-Agent\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36\";s:3:\"DNT\";s:1:\"1\";s:6:\"Accept\";s:118:\"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3\";s:15:\"Accept-Encoding\";s:17:\"gzip, deflate, br\";s:15:\"Accept-Language\";s:11:\"en,id;q=0.9\";}','','::1',1558659855,NULL,'0',0),
(54,'pegawai','get','a:10:{s:4:\"Host\";s:9:\"localhost\";s:10:\"Connection\";s:10:\"keep-alive\";s:13:\"Cache-Control\";s:9:\"max-age=0\";s:13:\"Authorization\";s:26:\"Basic YWRtaW46MTEyMjMzNDU=\";s:25:\"Upgrade-Insecure-Requests\";s:1:\"1\";s:10:\"User-Agent\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36\";s:3:\"DNT\";s:1:\"1\";s:6:\"Accept\";s:118:\"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3\";s:15:\"Accept-Encoding\";s:17:\"gzip, deflate, br\";s:15:\"Accept-Language\";s:11:\"en,id;q=0.9\";}','','::1',1558661212,NULL,'0',0),
(55,'pegawai','get','a:10:{s:4:\"Host\";s:9:\"localhost\";s:10:\"Connection\";s:10:\"keep-alive\";s:13:\"Cache-Control\";s:9:\"max-age=0\";s:13:\"Authorization\";s:26:\"Basic YWRtaW46MTEyMjMzNDU=\";s:25:\"Upgrade-Insecure-Requests\";s:1:\"1\";s:10:\"User-Agent\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36\";s:3:\"DNT\";s:1:\"1\";s:6:\"Accept\";s:118:\"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3\";s:15:\"Accept-Encoding\";s:17:\"gzip, deflate, br\";s:15:\"Accept-Language\";s:11:\"en,id;q=0.9\";}','','::1',1558661213,NULL,'0',0),
(56,'pegawai','get','a:10:{s:4:\"Host\";s:9:\"localhost\";s:10:\"Connection\";s:10:\"keep-alive\";s:13:\"Cache-Control\";s:9:\"max-age=0\";s:13:\"Authorization\";s:26:\"Basic YWRtaW46MTEyMjMzNDU=\";s:25:\"Upgrade-Insecure-Requests\";s:1:\"1\";s:10:\"User-Agent\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36\";s:3:\"DNT\";s:1:\"1\";s:6:\"Accept\";s:118:\"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3\";s:15:\"Accept-Encoding\";s:17:\"gzip, deflate, br\";s:15:\"Accept-Language\";s:11:\"en,id;q=0.9\";}','','::1',1558661230,NULL,'1',0),
(57,'pegawai','get','a:8:{s:4:\"Host\";s:9:\"localhost\";s:10:\"User-Agent\";s:78:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:66.0) Gecko/20100101 Firefox/66.0\";s:6:\"Accept\";s:63:\"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\";s:15:\"Accept-Language\";s:14:\"en-US,en;q=0.5\";s:15:\"Accept-Encoding\";s:13:\"gzip, deflate\";s:3:\"DNT\";s:1:\"1\";s:10:\"Connection\";s:10:\"keep-alive\";s:25:\"Upgrade-Insecure-Requests\";s:1:\"1\";}','','::1',1558661248,NULL,'1',0),
(58,'pegawai','get','a:8:{s:9:\"x-api-key\";s:13:\"GIZBerau@2019\";s:13:\"cache-control\";s:8:\"no-cache\";s:13:\"Postman-Token\";s:36:\"8d378bc0-0b96-4cad-8def-55be3ed8d147\";s:10:\"User-Agent\";s:20:\"PostmanRuntime/7.6.1\";s:6:\"Accept\";s:3:\"*/*\";s:4:\"Host\";s:9:\"localhost\";s:15:\"accept-encoding\";s:13:\"gzip, deflate\";s:10:\"Connection\";s:10:\"keep-alive\";}','','::1',1558661634,NULL,'1',0),
(59,'pegawai','get','a:10:{s:4:\"Host\";s:9:\"localhost\";s:10:\"Connection\";s:10:\"keep-alive\";s:13:\"Cache-Control\";s:9:\"max-age=0\";s:13:\"Authorization\";s:26:\"Basic YWRtaW46MTEyMjMzNDU=\";s:25:\"Upgrade-Insecure-Requests\";s:1:\"1\";s:10:\"User-Agent\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36\";s:3:\"DNT\";s:1:\"1\";s:6:\"Accept\";s:118:\"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3\";s:15:\"Accept-Encoding\";s:17:\"gzip, deflate, br\";s:15:\"Accept-Language\";s:11:\"en,id;q=0.9\";}','','::1',1558662989,NULL,'1',0),
(60,'pegawai','post','a:14:{s:12:\"Content-Type\";s:33:\"application/x-www-form-urlencoded\";s:9:\"x-api-key\";s:13:\"GIZBerau@2019\";s:13:\"cache-control\";s:8:\"no-cache\";s:13:\"Postman-Token\";s:36:\"c3344ecf-840a-40d6-a140-49c313305791\";s:13:\"Authorization\";s:26:\"Basic YWRtaW46MTEyMjMzNDU=\";s:10:\"User-Agent\";s:20:\"PostmanRuntime/7.6.1\";s:6:\"Accept\";s:3:\"*/*\";s:4:\"Host\";s:9:\"localhost\";s:15:\"accept-encoding\";s:13:\"gzip, deflate\";s:14:\"content-length\";s:2:\"46\";s:10:\"Connection\";s:10:\"keep-alive\";s:3:\"nik\";s:4:\"1122\";s:4:\"nama\";s:12:\"GIZ Berau 22\";s:13:\"jenis_kelamin\";s:1:\"L\";}','','::1',1558663433,NULL,'1',0),
(61,'pegawai','get','a:8:{s:9:\"x-api-key\";s:13:\"GIZBerau@2019\";s:13:\"cache-control\";s:8:\"no-cache\";s:13:\"Postman-Token\";s:36:\"86697df5-23d6-4409-a95f-077b15179733\";s:10:\"User-Agent\";s:20:\"PostmanRuntime/7.6.1\";s:6:\"Accept\";s:3:\"*/*\";s:4:\"Host\";s:9:\"localhost\";s:15:\"accept-encoding\";s:13:\"gzip, deflate\";s:10:\"Connection\";s:10:\"keep-alive\";}','','::1',1558663451,NULL,'1',0),
(62,'pegawai','get','a:10:{s:4:\"Host\";s:9:\"localhost\";s:10:\"Connection\";s:10:\"keep-alive\";s:13:\"Cache-Control\";s:9:\"max-age=0\";s:13:\"Authorization\";s:26:\"Basic YWRtaW46MTEyMjMzNDU=\";s:25:\"Upgrade-Insecure-Requests\";s:1:\"1\";s:10:\"User-Agent\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36\";s:3:\"DNT\";s:1:\"1\";s:6:\"Accept\";s:118:\"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3\";s:15:\"Accept-Encoding\";s:17:\"gzip, deflate, br\";s:15:\"Accept-Language\";s:11:\"en,id;q=0.9\";}','','::1',1558665671,NULL,'0',0),
(63,'pegawai','get','a:10:{s:4:\"Host\";s:9:\"localhost\";s:10:\"Connection\";s:10:\"keep-alive\";s:13:\"Cache-Control\";s:9:\"max-age=0\";s:13:\"Authorization\";s:26:\"Basic YWRtaW46MTEyMjMzNDU=\";s:25:\"Upgrade-Insecure-Requests\";s:1:\"1\";s:10:\"User-Agent\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36\";s:3:\"DNT\";s:1:\"1\";s:6:\"Accept\";s:118:\"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3\";s:15:\"Accept-Encoding\";s:17:\"gzip, deflate, br\";s:15:\"Accept-Language\";s:11:\"en,id;q=0.9\";}','','::1',1558665680,NULL,'0',0),
(64,'pegawai','get','a:10:{s:4:\"Host\";s:9:\"localhost\";s:10:\"Connection\";s:10:\"keep-alive\";s:13:\"Cache-Control\";s:9:\"max-age=0\";s:13:\"Authorization\";s:26:\"Basic YWRtaW46MTEyMjMzNDU=\";s:25:\"Upgrade-Insecure-Requests\";s:1:\"1\";s:10:\"User-Agent\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36\";s:3:\"DNT\";s:1:\"1\";s:6:\"Accept\";s:118:\"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3\";s:15:\"Accept-Encoding\";s:17:\"gzip, deflate, br\";s:15:\"Accept-Language\";s:11:\"en,id;q=0.9\";}','','::1',1558665687,NULL,'0',0),
(65,'pegawai','get','a:10:{s:4:\"Host\";s:9:\"localhost\";s:10:\"Connection\";s:10:\"keep-alive\";s:13:\"Cache-Control\";s:9:\"max-age=0\";s:13:\"Authorization\";s:26:\"Basic YWRtaW46MTEyMjMzNDU=\";s:25:\"Upgrade-Insecure-Requests\";s:1:\"1\";s:10:\"User-Agent\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36\";s:3:\"DNT\";s:1:\"1\";s:6:\"Accept\";s:118:\"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3\";s:15:\"Accept-Encoding\";s:17:\"gzip, deflate, br\";s:15:\"Accept-Language\";s:11:\"en,id;q=0.9\";}','','::1',1558665930,NULL,'0',0),
(66,'pegawai','get','a:9:{s:9:\"x-api-key\";s:9:\"CODEX@123\";s:13:\"cache-control\";s:8:\"no-cache\";s:13:\"Postman-Token\";s:36:\"20bec670-bbd8-4488-8083-6357fc0d455d\";s:13:\"Authorization\";s:26:\"Basic YWRtaW46MTEyMjMzNDU=\";s:10:\"User-Agent\";s:20:\"PostmanRuntime/7.6.1\";s:6:\"Accept\";s:3:\"*/*\";s:4:\"Host\";s:9:\"localhost\";s:15:\"accept-encoding\";s:13:\"gzip, deflate\";s:10:\"Connection\";s:10:\"keep-alive\";}','','::1',1558666407,NULL,'0',0),
(67,'pegawai','get','a:9:{s:9:\"x-api-key\";s:9:\"CODEX@123\";s:13:\"cache-control\";s:8:\"no-cache\";s:13:\"Postman-Token\";s:36:\"480feef9-75d4-45b7-a352-d0758281ebdd\";s:13:\"Authorization\";s:26:\"Basic YWRtaW46MTEyMjMzNDU=\";s:10:\"User-Agent\";s:20:\"PostmanRuntime/7.6.1\";s:6:\"Accept\";s:3:\"*/*\";s:4:\"Host\";s:9:\"localhost\";s:15:\"accept-encoding\";s:13:\"gzip, deflate\";s:10:\"Connection\";s:10:\"keep-alive\";}','','::1',1558666449,NULL,'0',0),
(68,'pegawai','get','a:9:{s:9:\"x-api-key\";s:13:\"GIZBerau@2019\";s:13:\"cache-control\";s:8:\"no-cache\";s:13:\"Postman-Token\";s:36:\"383e6c93-a586-441b-8d88-3e6b67a17b97\";s:13:\"Authorization\";s:26:\"Basic YWRtaW46MTEyMjMzNDU=\";s:10:\"User-Agent\";s:20:\"PostmanRuntime/7.6.1\";s:6:\"Accept\";s:3:\"*/*\";s:4:\"Host\";s:9:\"localhost\";s:15:\"accept-encoding\";s:13:\"gzip, deflate\";s:10:\"Connection\";s:10:\"keep-alive\";}','GIZBerau@2019','::1',1558666481,NULL,'1',0);

/*Table structure for table `pegawai` */

DROP TABLE IF EXISTS `pegawai`;

CREATE TABLE `pegawai` (
  `nik` varchar(30) NOT NULL,
  `nama` varchar(200) DEFAULT NULL,
  `jenis_kelamin` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`nik`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `pegawai` */

insert  into `pegawai`(`nik`,`nama`,`jenis_kelamin`) values 
('1122','GIZ Berau 22','L'),
('112233','GIZ Berau','L'),
('64710524109900','Agus Tri Haryono','P');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
