<?php
class model_master extends CI_Model
{
	public function __construct() {
        parent::__construct();
        
        // Load the database library
        $this->load->database();
    }
	function get($table)
	{		
		$this->db->select('*');
		$this->db->from($table);

		$qr=$this->db->get();

		if($qr->num_rows()>0)
			return $qr->result();
		else
			return false;			
	}

	function get_by_id($table,$key)
	{		
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where($key);

		$qr=$this->db->get();

		if($qr->num_rows()>0)
			return $qr->row();
		else
			return false;			
	}

	function insert($table,$param)
	{	
		$this->db->trans_start();
		$this->db->insert($table,$param);
		$query = $this->db->last_query(); 		
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
			return false;
		else
			return true;			
	}

	function update($table,$param,$key)
	{
		$this->db->trans_start();
		$this->db->where($key);
		$this->db->update($table, $param); 
		/*$query = $this->db->last_query();
		debugLog($query);*/
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
			return false;
		else
			return true;
	}

	function delete($table,$key)
	{
		$this->db->trans_start();
		$this->db->where($key);
		$this->db->delete($table);
		//DELETE FROM pegawai WHERE nik = '124';
		//debugLog($this->db->last_query(),$d);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
			return false;
		else
			return true;
	}
}
?>