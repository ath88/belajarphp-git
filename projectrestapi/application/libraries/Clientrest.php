<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
class Clientrest {
	var $apiuser = "admin"; // you can change it
    var $apipass = "11223345"; // you can change it
    var $apikey = "GIZBerau@2019"; // you can change it
	
    public function GET($url)
    {
        // Create a new cURL resource
        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("X-API-KEY: " . $this->apikey));
        curl_setopt($ch, CURLOPT_USERPWD, $this->apiuser.":".$this->apipass);

        $result = curl_exec($ch);

        // Close cURL resource
        curl_close($ch);
        return $result;
    }

    public function POST($url,$param)
    {
        // Create a new cURL resource
        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("X-API-KEY: " . $this->apikey));
        curl_setopt($ch, CURLOPT_USERPWD, $this->apiuser.":".$this->apipass);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $param);

        $result = curl_exec($ch);

        // Close cURL resource
        curl_close($ch);
        return $result;
    }

    public function PUT($url,$param)
    {
        // Create a new cURL resource
        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-API-KEY: '.$this->apikey, 'Content-Type: application/x-www-form-urlencoded'));
        curl_setopt($ch, CURLOPT_USERPWD, $this->apiuser.":".$this->apipass);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($param));

        $result = curl_exec($ch);

        // Close cURL resource
        curl_close($ch);
        return $result;
    }

    public function DELETE($url,$param)
    {
        // Create a new cURL resource
        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-API-KEY: '.$this->apikey, 'Content-Type: application/x-www-form-urlencoded'));
        curl_setopt($ch, CURLOPT_USERPWD, $this->apiuser.":".$this->apipass);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($param));

        $result = curl_exec($ch);

        // Close cURL resource
        curl_close($ch);
        return $result;
    }
}
?>