<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clientpegawai extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->library('clientrest');
	}

	public function index()
	{
		$param = array(
			'nik'=>'54321',
			'nama'=>'TEST POST API',
			'jenis_kelamin'=>'L'
		);
		$response = $this->clientrest->POST('http://localhost/projectrestapi/index.php/pegawai/index/',$param);
		echo "Response POST:<br/>";
		print_r(json_decode($response));
		echo "<br/><br/>";

		$response = $this->clientrest->GET('http://localhost/projectrestapi/index.php/pegawai/');
		echo "Response GET:<br/>";
		print_r(json_decode($response));
		echo "<br/><br/>";

		$param = array(
			'id'=>'54321',
			'nik'=>'543211',
			'nama'=>'TEST POST API PUT',
			'jenis_kelamin'=>'L'
		);
		$response = $this->clientrest->PUT('http://localhost/projectrestapi/index.php/pegawai/index/',$param);
		echo "Response PUT:<br/>";
		print_r(json_decode($response));
		echo "<br/><br/>";

		$response = $this->clientrest->GET('http://localhost/projectrestapi/index.php/pegawai/index/id/543211/');
		echo "Response GET ID:<br/>";
		print_r(json_decode($response));
		echo "<br/><br/>";

		$param = array(
			'id'=>'543211'
		);
		$response = $this->clientrest->DELETE('http://localhost/projectrestapi/index.php/pegawai/index/',$param);
		echo "Response DELETE:<br/>";
		print_r(json_decode($response));
		echo "<br/><br/>";
	}
}