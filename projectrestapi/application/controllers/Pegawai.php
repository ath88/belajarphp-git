<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Pegawai extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->model('model_master');
    }

    private function response_message($msg,$coderesponse){
        $this->response([
            'status' => FALSE,
            'message' => $msg
        ], $coderesponse);

        //404 NOT_FOUND
        //400 BAD_REQUEST
        //502 BAD_GATEWAY
    }

    function index_get() {
        $id = $this->get('id');
        if ($id == '') {
            $datas = $this->model_master->get('pegawai');
        } else {
            $id_sanitize = preg_replace("/[^a-zA-Z0-9]/", "", $id);
            if($id!==$id_sanitize)
            {
                // Invalid id, set the response and exit.
                $this->response_message('Bad request',500);
            } else {                
                $key = array('nik'=>$id_sanitize);
                $datas = $this->model_master->get_by_id('pegawai',$key);
            }
        }
        if($datas!=false)
            $this->response($datas, 200);
        else
            $this->response_message('Data not found',404);
    }

    function index_post() {
        $param = array(
                    'nik' => $this->post('nik'),
                    'nama' => $this->post('nama'),
                    'jenis_kelamin' => $this->post('jenis_kelamin')
                );
        $proses = $this->model_master->insert('pegawai', $param);
        if ($proses) {
            $this->response($param, 200);
        } else {
            $this->response_message('Fail process',502);
        }
    }

    function index_put() {
        $id = $this->put('id');
        $param = array(
                    'nik' => $this->put('nik'),
                    'nama' => $this->put('nama'),
                    'jenis_kelamin' => $this->put('jenis_kelamin')
                );
        $key = array('nik'=>$id);
        $proses = $this->model_master->update('pegawai',$param,$key);
        if ($proses) {
            $this->response($param, 200);
        } else {
            $this->response_message('Fail process',502);
        }
    }

    function index_delete() {
        $id = $this->delete('id');
        $key = array('nik'=>$id);
        $proses = $this->model_master->delete('pegawai',$key);
        if ($proses) {
           $this->response_message('Success',200);
        } else {
            $this->response_message('Fail process',502);
        }
    }

}
?>