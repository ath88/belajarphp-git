/*
SQLyog Ultimate v12.4.3 (64 bit)
MySQL - 10.1.28-MariaDB : Database - pegawai
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`pegawai` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `pegawai`;

/*Table structure for table `history_jabatan` */

DROP TABLE IF EXISTS `history_jabatan`;

CREATE TABLE `history_jabatan` (
  `id_history` int(11) NOT NULL AUTO_INCREMENT,
  `nik` varchar(100) NOT NULL,
  `kode_jabatan` int(11) NOT NULL,
  `tmt_jabatan` date NOT NULL,
  PRIMARY KEY (`id_history`),
  KEY `kode_jabatan` (`kode_jabatan`),
  KEY `nik` (`nik`),
  CONSTRAINT `history_jabatan_ibfk_1` FOREIGN KEY (`kode_jabatan`) REFERENCES `jabatan` (`kodejabatan`) ON UPDATE CASCADE,
  CONSTRAINT `history_jabatan_ibfk_2` FOREIGN KEY (`nik`) REFERENCES `pegawai` (`nik`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `history_jabatan` */

insert  into `history_jabatan`(`id_history`,`nik`,`kode_jabatan`,`tmt_jabatan`) values 
(1,'124',1,'2019-05-01');

/*Table structure for table `jabatan` */

DROP TABLE IF EXISTS `jabatan`;

CREATE TABLE `jabatan` (
  `kodejabatan` int(11) NOT NULL AUTO_INCREMENT,
  `namajabatan` varchar(100) NOT NULL,
  PRIMARY KEY (`kodejabatan`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `jabatan` */

insert  into `jabatan`(`kodejabatan`,`namajabatan`) values 
(1,'Kepala Dinas'),
(2,'Kepala Bagian');

/*Table structure for table `pegawai` */

DROP TABLE IF EXISTS `pegawai`;

CREATE TABLE `pegawai` (
  `nik` varchar(30) NOT NULL,
  `nama` varchar(200) DEFAULT NULL,
  `jenis_kelamin` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`nik`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `pegawai` */

insert  into `pegawai`(`nik`,`nama`,`jenis_kelamin`) values 
('124','jetty','P');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
